export { default as Button } from "./Button.svelte";
export { default as Counter } from "./Counter.svelte";
export { default as Input } from "./Input.svelte";
export { default as Score } from "./Score.svelte";
