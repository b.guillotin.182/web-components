// import the components HERE to be bundled ...
import { Counter, Button, Input, Score } from "./lib/index.js";

// //// NE PAS PRENDRE EN COMPTE LA SUITE ... dans le build
// pour le bundle de la lib
// ----->
// npm run build


// Sinon décommenter la suite pour tester vos composants
// ----->
// npm run dev
import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'score'
	}
});

export default app;
