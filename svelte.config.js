import { vitePreprocess } from '@sveltejs/vite-plugin-svelte'

export default {
  // Consult https://svelte.dev/docs#compile-time-svelte-preprocess
  // for more information about preprocessors
  // Main purpose -->  prePocess files type TypeScript, PostCSS, SCSS, and Less.
  preprocess: vitePreprocess(),
}
