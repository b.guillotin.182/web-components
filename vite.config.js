import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";
import rollupNodePolyFill from "rollup-plugin-node-polyfills";

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    minify: false,
    rollupOptions: {
      plugins: [rollupNodePolyFill()],
      input: "src/main.js",
      output: {
        chunkFileNames: "chunk.js",
        format: "esm",
      },
    },
  },
  plugins: [
    svelte({
      compilerOptions: {
        customElement: true,
      },
    }),
  ],
});
